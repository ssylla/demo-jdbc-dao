package fr.sdv.rennes.m1;

import fr.sdv.rennes.m1.dal.PersistenceManager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

public class AppInjection {
	
	public static void main( String[] args ) {
	
		login();
	}
	
	public static void login() {
		
		Scanner sc = new Scanner( System.in );
		System.out.println("Bienvenue, merci de vous identifier");
		System.out.print("Login : ");
		String login = sc.nextLine();
		System.out.print("Mot de passe : ");
		String pwd = sc.nextLine();
		
		try ( Connection cnx = PersistenceManager.getConnection();
			  PreparedStatement pst = cnx.prepareStatement("SELECT * FROM utilisateur WHERE LOGIN=? AND PWD=?" ) ) {
			
			pst.setString( 1, login );
			pst.setString( 2, pwd );
			
			try( ResultSet rs = pst.executeQuery( ) ) {
				if (rs.next()) {
					System.out.println("Utilisateur authentifié : "+rs.getString( "LOGIN" ));
				} else {
					System.out.println("Erreur d'identification");
				}
			}
			
		} catch ( SQLException e ) {
			System.err.println(e.getMessage());
		}
		
	}
}
