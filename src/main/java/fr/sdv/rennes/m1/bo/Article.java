package fr.sdv.rennes.m1.bo;

public class Article {
	private int id;
	private String ref;
	private String libelle;
	private double prix;
	private Fournisseur fournisseur;
	
	public Article() {
	}
	
	public Article( String ref, String libelle, double prix ) {
		this.ref = ref;
		this.libelle = libelle;
		this.prix = prix;
	}
	
	public Article( int id, String ref, String libelle, double prix, Fournisseur fournisseur ) {
		this.id = id;
		this.ref = ref;
		this.libelle = libelle;
		this.prix = prix;
		this.fournisseur = fournisseur;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId( int id ) {
		this.id = id;
	}
	
	public String getRef() {
		return ref;
	}
	
	public void setRef( String ref ) {
		this.ref = ref;
	}
	
	public String getLibelle() {
		return libelle;
	}
	
	public void setLibelle( String libelle ) {
		this.libelle = libelle;
	}
	
	public double getPrix() {
		return prix;
	}
	
	public void setPrix( double prix ) {
		this.prix = prix;
	}
	
	public Fournisseur getFournisseur() {
		return fournisseur;
	}
	
	public void setFournisseur( Fournisseur fournisseur ) {
		this.fournisseur = fournisseur;
	}
	
	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder( "Artcile{" );
		sb.append( "id=" ).append( id );
		sb.append( ", ref='" ).append( ref ).append( '\'' );
		sb.append( ", libelle='" ).append( libelle ).append( '\'' );
		sb.append( ", prix=" ).append( prix );
		sb.append( ", fournisseur=" ).append( fournisseur );
		sb.append( '}' );
		return sb.toString();
	}
}
