package fr.sdv.rennes.m1;

import fr.sdv.rennes.m1.bo.Fournisseur;
import fr.sdv.rennes.m1.dal.FournisseurDAOJDBC;
import fr.sdv.rennes.m1.dal.IDAO;

import java.sql.SQLException;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) {
    
        try {
            Fournisseur fournisseurASupp = new Fournisseur( 3, "Dubois & fils" );
            IDAO<Integer, Fournisseur> dao = new FournisseurDAOJDBC();
            dao.delete( fournisseurASupp );
        }catch ( SQLException e ) {
            System.out.println(e.getMessage());
        }
    }
}
