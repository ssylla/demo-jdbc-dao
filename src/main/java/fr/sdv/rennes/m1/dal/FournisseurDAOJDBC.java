package fr.sdv.rennes.m1.dal;

import fr.sdv.rennes.m1.bo.Fournisseur;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class FournisseurDAOJDBC implements IDAO<Integer, Fournisseur> {
	
	private static final String CREATE_QUERY = "INSERT INTO fournisseur(NOM) VALUES(?)";
	private static final String FIND_ID_QUERY = "SELECT * FROM fournisseur WHERE ID = ?";
	private static final String FIND_ALL_QUERY = "SELECT * FROM fournisseur";
	private static final String UPDATE_QUERY = "UPDATE fournisseur SET NOM=? WHERE ID=?";
	private static final String UPDATE_NAME_QUERY = "UPDATE fournisseur SET NOM=? WHERE NOM=?";
	private static final String DELETE_ID_QUERY = "DELETE FROM fournisseur WHERE ID=?";
	
	@Override
	public void create( Fournisseur fournisseur ) throws SQLException {
		Connection cnx = PersistenceManager.getConnection();
		try ( PreparedStatement pst = cnx.prepareStatement( CREATE_QUERY,Statement.RETURN_GENERATED_KEYS ) ) {
			pst.setString( 1, fournisseur.getNom() );
			pst.executeUpdate();
			try( ResultSet  rs = pst.getGeneratedKeys() ) {
				if (rs.next()) {
					fournisseur.setId( rs.getInt( 1 ) );
				}
			}
		}
	}
	
	@Override
	public Fournisseur findById( Integer id ) throws SQLException {
		Connection cnx = PersistenceManager.getConnection();
		Fournisseur fournisseur = null;
		try(PreparedStatement pst = cnx.prepareStatement(FIND_ID_QUERY)) {
			pst.setInt( 1, id );
			try (ResultSet rs = pst.executeQuery()) {
				if (rs.next()) {
					fournisseur = new Fournisseur(rs.getInt( "ID" ), rs.getString( "NOM" ));
				}
			}
		}
		return fournisseur;
	}
	
	@Override
	public List<Fournisseur> findAll() throws SQLException {
		
		Connection cnx = PersistenceManager.getConnection();
		List<Fournisseur> fournisseurs = new ArrayList<>();
		try(Statement st = cnx.createStatement();
			ResultSet rs = st.executeQuery( FIND_ALL_QUERY )) {
			
			while ( rs.next() ) {
				fournisseurs.add( new Fournisseur(rs.getInt( "ID" ), rs.getString( "NOM" )) );
			}
		}
		return fournisseurs;
	}
	
	@Override
	public void update( Fournisseur fournisseur ) throws SQLException {
		Connection cnx = PersistenceManager.getConnection();
		try ( PreparedStatement pst = cnx.prepareStatement( UPDATE_QUERY ) ) {
			pst.setString( 1, fournisseur.getNom() );
			pst.setInt( 2, fournisseur.getId() );
			pst.executeUpdate();
		}
	}
	
	@Override
	public void deleteById( Integer id ) throws SQLException {
		Connection cnx = PersistenceManager.getConnection();
		try ( PreparedStatement pst = cnx.prepareStatement( DELETE_ID_QUERY ) ) {
			pst.setInt( 1, id );
			pst.executeUpdate();
		}
	}
	
	@Override
	public void delete( Fournisseur fournisseur ) throws SQLException {
		deleteById( fournisseur.getId() );
	}
}
