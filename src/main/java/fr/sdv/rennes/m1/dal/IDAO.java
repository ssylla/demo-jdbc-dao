package fr.sdv.rennes.m1.dal;

import java.sql.SQLException;
import java.util.List;

public interface IDAO<ID, T> {
	
	void create(T object) throws SQLException;
	T findById(ID id) throws SQLException;;
	List<T> findAll() throws SQLException;;
	void update(T object) throws SQLException;
	void deleteById(ID id) throws SQLException;
	void delete(T object) throws SQLException;
}
