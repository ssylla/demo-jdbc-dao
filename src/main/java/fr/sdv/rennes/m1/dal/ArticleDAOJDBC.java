package fr.sdv.rennes.m1.dal;

import fr.sdv.rennes.m1.bo.Article;

import java.sql.SQLException;
import java.util.List;

public class ArticleDAOJDBC implements IDAO<Integer, Article> {
	@Override
	public void create( Article object ) throws SQLException {
	
	}
	
	@Override
	public Article findById( Integer integer ) throws SQLException {
		return null;
	}
	
	@Override
	public List<Article> findAll() throws SQLException {
		return null;
	}
	
	@Override
	public void update( Article object ) throws SQLException {
	
	}
	
	@Override
	public void deleteById( Integer integer ) throws SQLException {
	
	}
	
	@Override
	public void delete( Article object ) {
	
	}
}
