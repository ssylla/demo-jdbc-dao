package fr.sdv.rennes.m1.dal;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ResourceBundle;

public final class PersistenceManager {
	
	private static final String DB_URL;
	private static final String DB_USER;
	private static final String DB_PWD;
	
	private static Connection connection;
	
	static {
		ResourceBundle bundle = ResourceBundle.getBundle( "db" );
		DB_URL = bundle.getString( "jdbc.db.url" );
		DB_USER = bundle.getString( "jdbc.db.user" );
		DB_PWD = bundle.getString( "jdbc.db.pwd" );
	}
	
	private PersistenceManager() {}
	
	public static Connection getConnection() throws SQLException {
		if ( null == connection || connection.isClosed() ) {
			connection = DriverManager.getConnection( DB_URL, DB_USER, DB_PWD );
		}
		return connection;
	}
	
	public static void closeConnection() throws SQLException {
		if ( null != connection && !connection.isClosed() ) {
			connection.close();
		}
	}
}
